# Copyright (c) 2013 Shalev Ben-David, Rati Gelashvili, Henry Yuen
# This code is released under the MIT license (see LICENSE file).

from PuncturedEncoder import PuncturedEncoder

from BGYDecoder import Decoder
from BGYDecoder import StrataDecoder
from BGYDecoder import StackDecoder

import BubbleDecoder

from SymbolMapper import SymbolMapper
import random
import math

# some constants:
k = 4
B = 256
d = 1
c = 8
precision = 10

#generate a random message of length m
m = 64

# Message to be encoded:
message = ""

#number of tail symbols
num_tail_symbols = 2
puncturing_schedule = [7,3,5,1,4,0,6,2]
#puncturing_schedule = [0,1,2,3,4,5,6,7]
num_passes = 1  #start at one
max_passes = 8
passes_before_decoding = 1

if __name__ == '__main__':
    for i in range(m/8):
        message += chr(random.choice(range(256)))
    
    mapper = SymbolMapper(c, precision)
    map_func = lambda value: mapper.map(value)
    
    #Instantiate an encoder
    print 'Message string: "%s"' % message
    print 'Message hex:', message.encode("hex")
    encoder = PuncturedEncoder(k, map_func, message,num_tail_symbols,puncturing_schedule)
    
    # spine length
    n = 8 * len(message)
    spine_length = (n + (k - 1)) / k
    
    # get average signal power
    signal_power = mapper.get_signal_average_power()
    
    # what is the noise power and standard deviation when SNR is 10dB?
    SNR_dB = 10
    noise_power = signal_power/math.pow(10,SNR_dB/10.0)
    
    noise_std_dev = math.sqrt(noise_power)
    
    # instantiate decoder
    #'''
    decoder = Decoder(k,  decoder = StackDecoder(k, map_func, noise_std_dev,metric = "prob", bias = 12), \
                                                  slow_start = False, \
                                                  slow_start_parameters = {  \
                                                                          'max_depth': m/k,     \
                                                                          'lookahead': d,       \
                                                                          'beam': B  }      )
    #'''
    #decoder = BubbleDecoder.Decoder(k, B, d, map_func)
    decoded_flag = False
    
    symbols = {}
    original_symbols = {}
    total_cycles = 0
    num_decoding_attempts = 0
    
    while num_passes < max_passes and decoded_flag is False:
        #perform a pass according to the schedule
        print "==Performing pass number ",num_passes,"=="
        
        for subpass_index in puncturing_schedule:
            print "Performing subpass ",subpass_index
            subpass_symbols, tail_symbols = encoder.get_next_subpass()
            for index, symbol in subpass_symbols.iteritems():
                #add gaussian noise to simulate an AWGN channel
                noisy_symbol = int(symbol + random.gauss(0, noise_std_dev) + 0.5)
                if symbols.has_key(index) is False:
                    symbols[index] = []
                    original_symbols[index] = []
                
                symbols[index].append(noisy_symbol)
                original_symbols[index].append(symbol)
                
            if len(tail_symbols) > 0:
                #add gaussian noise to the tail symbols
                noisy_tail_symbols = [sym + random.gauss(0,noise_std_dev) for sym in tail_symbols]
                noisy_tail_symbols = [int(sym + 0.5) for sym in noisy_tail_symbols]
                symbols[spine_length - 1].extend(noisy_tail_symbols)
                original_symbols[spine_length - 1].extend(tail_symbols)
                
            #flatten out the dict to a list
            num_symbols = 0
            symbols_list = []
            original_symbols_list = []
            
            for i in range(spine_length):
                if symbols.has_key(i) is False:
                    symbols_list.append([])
                    original_symbols_list.append([])
                else:
                    symbols_list.append(symbols[i])
                    original_symbols_list.append(original_symbols[i])
                    
                num_symbols += len(symbols_list[i])
                
            #print symbols_list
            cycle_limit = 5000
            
            if num_passes > passes_before_decoding:
                attempts_threshold = 0
                if num_passes < 3:
                    attempts_threshold = 500
                else:
                    attempts_threshold = 500

                
                #fake_decoder = StrataDecoder(k, map_func, noise_std_dev,metric = "")
                #print "**Orig: ", original_symbols_list
                #print "**Noisy: ", symbols_list
                #print "**", fake_decoder.compute_path_cost(original_symbols_list, symbols_list)
                
                attempts_threshold = 5000
                ok, most_likely, cycles = decoder.decode(symbols_list, message, attempts_threshold, cycle_limit)
                
                #cycles = 1
                num_decoding_attempts += attempts_threshold
                total_cycles += cycles
                if ok:
                    print "Decoded successfully!"
                    print "total number of symbols sent", num_symbols
                    print "total cycles: ", total_cycles
                    print "total decoding attempts: ", num_decoding_attempts
                    #See if we decoded correctly
                    decoded_flag = True
                    break
                
        num_passes += 1
    
    
    