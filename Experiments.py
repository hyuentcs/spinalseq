# Copyright (c) 2013 Shalev Ben-David, Rati Gelashvili, Henry Yuen
# This code is released under the MIT license (see LICENSE file).

from PuncturedEncoder import PuncturedEncoder

from BGYDecoder import Decoder
from BGYDecoder import StrataDecoder
from BGYDecoder import StackDecoder

import BubbleDecoder

from SymbolMapper import SymbolMapper
import random
import math


def run_experiment(params):
    SNR_dB = params['SNR_dB']

    # some constants:
    k = params['k']
    B = params['B']
    d = params['d']
    c = params['c']
    precision = params['precision']

    #generate a random message of length m
    m = params['m']

    # Message to be encoded:
    message = ""

    #number of tail symbols
    num_tail_symbols = params['num_tail_symbols']
    puncturing_schedule = [7,3,5,1,4,0,6,2]
    #puncturing_schedule = [0,1,2,3,4,5,6,7]
    num_passes = 1  #start at one
    max_passes = 6
    passes_before_decoding = params['passes_before_decoding']

    for i in range(m/8):
        message += chr(random.choice(range(256)))
    
    mapper = SymbolMapper(c, precision)
    map_func = lambda value: mapper.map(value)
    
    #Instantiate an encoder
    print 'Message string: "%s"' % message
    print 'Message hex:', message.encode("hex")
    encoder = PuncturedEncoder(k, map_func, message,num_tail_symbols,puncturing_schedule)
    
    # spine length
    n = 8 * len(message)
    spine_length = (n + (k - 1)) / k
    
    # get average signal power
    signal_power = mapper.get_signal_average_power()
    
    # what is the noise power and standard deviation when SNR is 10dB?
    #SNR_dB = 10
    noise_power = signal_power/math.pow(10,SNR_dB/10.0)
    
    noise_std_dev = math.sqrt(noise_power)
    
    # instantiate decoder
    #'''
    
    decoder_type = params['decoder_type']
    
    if decoder_type == 'Strata':    
        decoder = Decoder(k,  decoder = StrataDecoder(k, map_func, noise_std_dev,metric = params['metric']), \
                                                      slow_start = False, \
                                                      slow_start_parameters = {  \
                                                                              'max_depth': m/k,     \
                                                                              'lookahead': d,       \
                                                                              'beam': B  }      )
    elif decoder_type == "Stack":
        b = noise_std_dev * 0.015 + 3.6
        decoder = Decoder(k,  decoder = StackDecoder(k, map_func, noise_std_dev,metric = params['metric'], bias = b), \
                                                      slow_start = False, \
                                                      slow_start_parameters = {  \
                                                                              'max_depth': m/k,     \
                                                                              'lookahead': d,       \
                                                                              'beam': B  }      )
    elif decoder_type == "Bubble":
        decoder = BubbleDecoder.Decoder(k, B, d, map_func)
        

    decoded_flag = False
    
    symbols = {}
    original_symbols = {}
    total_cycles = 0
    num_decoding_attempts = 0
    num_symbols = 0
    
    while num_passes < max_passes and decoded_flag is False:
        #perform a pass according to the schedule
        print "==Performing pass number ",num_passes,"=="
        
        for subpass_index in puncturing_schedule:
            print "Performing subpass ",subpass_index
            subpass_symbols, tail_symbols = encoder.get_next_subpass()
            for index, symbol in subpass_symbols.iteritems():
                #add gaussian noise to simulate an AWGN channel
                noisy_symbol = int(symbol + random.gauss(0, noise_std_dev) + 0.5)
                if symbols.has_key(index) is False:
                    symbols[index] = []
                    original_symbols[index] = []
                
                symbols[index].append(noisy_symbol)
                original_symbols[index].append(symbol)
                
            if len(tail_symbols) > 0:
                #add gaussian noise to the tail symbols
                noisy_tail_symbols = [sym + random.gauss(0,noise_std_dev) for sym in tail_symbols]
                noisy_tail_symbols = [int(sym + 0.5) for sym in noisy_tail_symbols]
                symbols[spine_length - 1].extend(noisy_tail_symbols)
                original_symbols[spine_length - 1].extend(tail_symbols)
                
            #flatten out the dict to a list
            num_symbols = 0
            symbols_list = []
            original_symbols_list = []
            
            for i in range(spine_length):
                if symbols.has_key(i) is False:
                    symbols_list.append([])
                    original_symbols_list.append([])
                else:
                    symbols_list.append(symbols[i])
                    original_symbols_list.append(original_symbols[i])
                    
                num_symbols += len(symbols_list[i])
                
            #print symbols_list
            cycle_limit = 5000
            
            if num_passes > passes_before_decoding+1 or (num_passes == passes_before_decoding+1 and subpass_index%2 == 0):
                attempts_threshold = 0
                if num_passes <= 1:
                    cycle_limit = 2000
                    attempts_threshold = 100
                else:
                    cycle_limit = 5000
                    attempts_threshold = 256

                attempts_threshold = cycle_limit
                ok, most_likely, cycles = decoder.decode(symbols_list, message, attempts_threshold, cycle_limit)
                
                #cycles = 1
                num_decoding_attempts += attempts_threshold
                total_cycles += cycles
                if ok:
                    print "SNR: ",SNR_dB
                    print "Decoded successfully!"
                    print "total number of symbols sent", num_symbols
                    print "total cycles: ", total_cycles
                    print "total decoding attempts: ", num_decoding_attempts
                    #See if we decoded correctly
                    decoded_flag = True
                    return (num_symbols, total_cycles)
                
        num_passes += 1
    
    return (-1,-1)


if __name__ == '__main__':
    import pickle
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("decoder_type",help="Type of decoder: Strata, Stack, Bubble")
    parser.add_argument("-o","--output_prefix")
    parser.add_argument("-m","--metric")
    
    args = parser.parse_args()
    
    # some constants:
    k = 4
    B = 256
    d = 1
    c = 6
    precision = 10

    #generate a random message of length m
    m = 64

    # Message to be encoded:
    message = ""

    #number of tail symbols
    num_tail_symbols = 2
    passes_before_decoding = 2
    metric = 'l2' #l2
    if args.metric:
        metric = args.metric

    bias = 0
    #decoder_type = "Bubble"
    decoder_type = args.decoder_type
    output_prefix = ""
    if args.output_prefix:
        output_prefix = args.output_prefix
    
    num_experiments_per_SNR = 500
    output_fname = 'results/%s-%s' % (output_prefix,decoder_type)
    SNR_list = [35,25,15,10,5]
    
    for j in range(num_experiments_per_SNR):
        results = {}
        for SNR_dB in SNR_list:
            if SNR_dB < 15: 
                passes_before_decoding = 1
            elif SNR_dB <= 25:
                passes_before_decoding = 0
            else:
                passes_before_decoding = 0

            params = {'SNR_dB': SNR_dB, \
                        'k': k,         \
                        'B': B,         \
                        'd': d,         \
                        'c': c,         \
                        'precision': precision,  \
                        'm': m,         \
                        'num_tail_symbols': num_tail_symbols,   \
                        'passes_before_decoding': passes_before_decoding, \
                        'decoder_type': decoder_type,               \
                        'metric': metric,                           \
                        'bias': bias}
                        
        
            (num_symbols,total_cycles) = run_experiment(params)
            results[SNR_dB] = (num_symbols,total_cycles)
            #write results to file
            pickle.dump(results, open('%s-%d' % (output_fname,j),'wb'))
