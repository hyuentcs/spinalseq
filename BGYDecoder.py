from Hash import hash_func
from RNG import RNG
import random
import heapq
import numpy
import scipy
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from scipy import integrate
from scipy import special

def integrate_gaussian(a,b, mu, sigma):
    def Phi(z):
        return 0.5*(1 + scipy.special.erf((z - mu)/(sigma*math.sqrt(2))))
        
    return Phi(b) - Phi(a)
    
def emit_log_prob(noise_std, spinal_val, output):
    """
    Calculate the probability of seeing output symbol "output"
    given the spine_value (mapped to rng mapped to constellation), using our estimate of the noise
    """
    
    prob = integrate_gaussian(output-1,output+1,spinal_val,noise_std)
    if prob == 0:
        return -numpy.inf
    
    return numpy.log(prob)
    
class StrataDecoder:
    def __init__(self, k, map_func, noise_std, metric="prob"):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.map_func = map_func
        self.noise_std = noise_std
        self.metric = metric
        
    def reset(self):
        pass

    def select_path(self, level_heaps, favored_level = -1):
        level_set = [i for i in range(len(level_heaps)) if len(level_heaps[i]) > 0]
        index_max = len(level_set)
        level_index = self._get_random_level(index_max)-1
        level = level_set[level_index]
        if favored_level != -1:
            level = favored_level
        
        best_path = heapq.heappop(level_heaps[level])
        
        return best_path

    def _get_random_level(self,dmax):
        if dmax == 0:
            return 0
        
        exp = 1
        sumset = [i**exp for i in range(1,dmax+1)]
        s = numpy.sum(sumset)
        sample = random.uniform(1,s)
        lb = 1
        for i in range(1,dmax+2):
            lb += i**exp
            if sample < lb:
                return i


    def explore_path(self, best_path, symbols):
        (path_metric, spine_value, path, path_length) = best_path
        
        new_paths = []
        # For each possible message block (2^k options)
        for edge in xrange( 1 << self.k ):
            # Calculate the new spine value
            new_spine_value = hash_func(spine_value, edge)
            
            # Get an RNG for the new spine value
            rng = RNG(new_spine_value)
            
            # Go over all received symbols, and compute the edge metric
            edge_cost = 0
            
            for received_symbol in symbols[path_length]:
                # What the transmitter would have produced if it had this spine value
                node_symbol = self.map_func(rng.next())
                edge_cost += self._compute_edge_metric(node_symbol, received_symbol)
                
            # The new path metric is the sum of all edges from the root
            new_path_metric = path_metric + edge_cost
            new_paths.append( (new_path_metric, new_spine_value, path + [edge], path_length + 1) )
            
        return new_paths

    def _compute_edge_metric(self, node_symbol, received_symbol):
        #what's the metric?
        cost = 0
        if self.metric == "prob":
            cost = -emit_log_prob(self.noise_std, node_symbol, received_symbol)
        elif self.metric == "l2":
            distance = node_symbol - received_symbol
            cost = distance*distance
        else:
            #throw an error
            raise RuntimeError, "Unsupported edge metric:  %s" % self.metric
    
        return cost
        
    def generate_path_symbols(self, path, original_message_symbols):

        path_symbols = []        
        prev_spine_value = 0
        
        for i in range(len(original_message_symbols)):
            edge = path[i]
            spine_value = hash_func(prev_spine_value, edge)
            prev_spine_value = spine_value
            
            rng = RNG(spine_value)
            
            path_symbols.append([])
            for j in range(len(original_message_symbols[i])):
                symbol = self.map_func(rng.next())
                path_symbols[i].append(symbol)
                

        return path_symbols
    
    def compute_path_cost(self, original_message_symbols, received_message_symbols):
        cost = 0
        for i in range(len(original_message_symbols)):
            for j in range(len(original_message_symbols[i])):
                original_symbol = original_message_symbols[i][j]
                received_symbol = received_message_symbols[i][j]
                cost += self._compute_edge_metric(original_symbol, received_symbol)
        
        return cost
        

class StackDecoder:
    def __init__(self, k, map_func, noise_std, metric="prob", bias = 12):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.map_func = map_func
        self.noise_std = noise_std
        self.metric = metric
        self.bias = bias
        self.edge_cost_average = 0
        self.edge_cost_count = 0
        
    def reset(self):
        self.edge_cost_average = 0
        self.edge_cost_count = 0
        
    def select_path(self, level_heaps):        
        #best_path = max(stack, key = lambda x: x[0])      
        num_levels = len(level_heaps)
        non_empty_levels = [i for i in range(num_levels) if len(level_heaps[i]) > 0]
        best_level = min(non_empty_levels, key = lambda x: level_heaps[x][0][0])   
        best_path = heapq.heappop(level_heaps[best_level])
        return best_path
        
    def explore_path(self, best_path, symbols):
        (path_metric, spine_value, path, path_length) = best_path
        
        new_paths = []
        # For each possible message block (2^k options)
        for edge in xrange( 1 << self.k ):
            # Calculate the new spine value
            new_spine_value = hash_func(spine_value, edge)
            
            # Get an RNG for the new spine value
            rng = RNG(new_spine_value)
            
            # Go over all received symbols, and compute the edge metric
            edge_cost = 0
            
            for received_symbol in symbols[path_length]:
                # What the transmitter would have produced if it had this spine value
                node_symbol = self.map_func(rng.next())
                edge_cost += self._compute_edge_metric(node_symbol, received_symbol)
                
            # The new path metric is the sum of all edges from the root
            new_path_metric = path_metric + edge_cost - self.bias * len(symbols[path_length])
            
            new_paths.append( (new_path_metric, new_spine_value, path + [edge], path_length + 1) )
            
        return new_paths

    def generate_path_symbols(self, path, original_message_symbols):

        path_symbols = []        
        prev_spine_value = 0
        
        for i in range(len(original_message_symbols)):
            edge = path[i]
            spine_value = hash_func(prev_spine_value, edge)
            prev_spine_value = spine_value
            
            rng = RNG(spine_value)
            
            path_symbols.append([])
            for j in range(len(original_message_symbols[i])):
                symbol = self.map_func(rng.next())
                path_symbols[i].append(symbol)
                

        return path_symbols
    
    def compute_path_cost(self, original_message_symbols, received_message_symbols):
        cost = 0
        for i in range(len(original_message_symbols)):
            for j in range(len(original_message_symbols[i])):
                original_symbol = original_message_symbols[i][j]
                received_symbol = received_message_symbols[i][j]
                cost += self._compute_edge_metric(original_symbol, received_symbol)
        
        return cost
            
    def _compute_edge_metric(self, node_symbol, received_symbol):
        #what's the metric?
        cost = 0
        #keep track of the average cost
        
        if self.metric == "prob":
            cost = -emit_log_prob(self.noise_std, node_symbol, received_symbol)
            
        elif self.metric == "l2":
            distance = node_symbol - received_symbol
            cost = (distance*distance) #+ self.noise_std**2
        else:
            #throw an error
            raise RuntimeError, "Unsupported edge metric:  %s" % self.metric
    
        #this is the bias for the Fano metric
        if numpy.isfinite(cost):
            self.edge_cost_average = (self.edge_cost_count*self.edge_cost_average + cost)/(1.0 * self.edge_cost_count + 1)
            self.edge_cost_count += 1
        
        return cost    

class Decoder(object):
    """
    Spinal decoder.
    
    To use this decoder to decode a message with a spine with n/k values, call
        advance() n/k times. On the i'th time, supplied parameter should be 
        a list of all symbols derived from the i'th spine value (ie using 
        encoder.get_symbol(i)).
         
    @note see advance() for assumption on inputs.
    """

    def __init__(self, k, decoder, slow_start = False, slow_start_parameters = None):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.decoder = decoder
        self.slow_start = slow_start
        self.slow_start_parameters = slow_start_parameters
        self.slow_start_current_depth = 0
        self.num_paths = 0
        
    def decode(self, symbols, original_message, search_attempts_threshold, cycle_limit):
        """
        @param original_message             this is the original message to compare with (can be CRC or the actual original message)
        @param search_attempts_threshold    this is the number of search attempts before the decoder aborts and asks for more symbols
        @return a boolean and string, the first value indicating whether the search was successful, and the second the most likely message to have been transmitted from 
            the wavefront.
        """
        
        self.num_paths = 1
        num_spine_values = len(symbols)
        level_heaps = {}
        for i in range(num_spine_values+1):
            level_heaps[i] = []

        level_heaps[0].append((0,0,[],0))
        heapq.heappush(level_heaps[0],(0,0,[],0))
        
        best_path = None
        num_search_attempts = 0
        cycles = 0
        #reset slow start
        self.slow_start_current_depth = 0
        self.decoder.reset()
        
        favored_level = -1
        while self.num_paths > 0 and cycles < cycle_limit:
            cycles += 1
            favored_level = -1
            
            #see if we're still in the slow-start phase
            if self.slow_start and self.slow_start_current_depth <= self.slow_start_parameters['max_depth'] - 1:
                stack = self._slow_start(stack, symbols)
                #fig = plt.plot([p[0] for p in stack])
                
                #plt.savefig('test.png')
                
                continue

            self.num_paths -= 1
            best_path = self.decoder.select_path(level_heaps, favored_level)
            
            #print best_path
            (path_metric, spine_value, path, path_length) = best_path
            #print path_length
            if path_length == num_spine_values: 
                #path_symbols = self.decoder.generate_path_symbols(path, symbols)
                #print path_symbols
                #print self.decoder.compute_path_cost(path_symbols, symbols)
                #print path_metric, path_length, num_spine_values
                
                candidate_message = self._decode_message(path)
                message_len = len(candidate_message)
                #do a diff
                diff = max([i for i in range(message_len) if candidate_message[:i] == original_message[:i]])
                #print "Number of cycles so far", cycles, " Agreement %d/%d" % (diff+1,message_len)
                if candidate_message == original_message:
                    return (True, candidate_message, cycles)
                    break
                
                #otherwise, we failed
                num_search_attempts += 1
                if num_search_attempts >= search_attempts_threshold:
                    return (False, None, cycles)
                
                #exit()
                continue
            
            
            
            new_paths = self.decoder.explore_path(best_path, symbols)
            #stack.extend(new_paths)
            next_depth = path_length + 1
            prev_min = numpy.inf
            if len(level_heaps[next_depth]) > 0:
                prev_min = level_heaps[next_depth][0][0]
            
            for p in new_paths:
                assert(p[3] == next_depth)
                heapq.heappush(level_heaps[next_depth],p)
            
            if prev_min > level_heaps[next_depth][0][0]:
                favored_level = next_depth
            
            self.num_paths += len(new_paths)
            
            #stack.remove(best_path)
            
        return (False, None, cycles)

    #######################
    # internal functions
    #######################
    def _slow_start(self, stack, symbols):
        print "Performing slow start..."
        B = self.slow_start_parameters['beam']
        while self.slow_start_current_depth <= self.slow_start_parameters['max_depth'] - 1:
            next_depth = []
            for p in stack:
                (path_metric, spine_value, path, path_length) = p
                #expand the path
                new_paths = self.decoder.explore_path(p, symbols)
                next_depth.extend(new_paths)
                stack.remove(p)
            
            stack = next_depth
            
            #prune and take only the beam best paths
            stack.sort(key = lambda p: p[0], reverse = True)
            stack = stack[:B]
            self.slow_start_current_depth += 1
        
        return stack

    def _decode_message(self, path):
        # join the path, first element makes LSB, last element makes MSB
        message_as_number = 0
        for block in reversed(path):
            message_as_number = (message_as_number << self.k) | block

        # transform the integer representation into the binary message
        #??? + 7??
        n = (len(path) * self.k) / 8
        message = ""
        for i in xrange(n):
            message += chr(message_as_number & 0xFF)
            message_as_number >>= 8
            
        return message    
    


