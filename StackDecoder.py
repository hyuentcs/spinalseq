from Hash import hash_func
from RNG import RNG
import random
import numpy
import scipy
import scipy
import math

from scipy import integrate
from scipy import special

class Decoder(object):
    """
    Spinal decoder.
    
    To use this decoder to decode a message with a spine with n/k values, call
        advance() n/k times. On the i'th time, supplied parameter should be 
        a list of all symbols derived from the i'th spine value (ie using 
        encoder.get_symbol(i)).
         
    @note see advance() for assumption on inputs.
    """

    def __init__(self, k, map_func, noise_std, metric="prob"):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.map_func = map_func
        self.noise_std = noise_std
        self.metric = metric
        self.edge_cost_average = 0
        self.edge_cost_count = 0
    
    def decode(self, symbols, original_message, search_attempts_threshold):
        """
        @param original_message             this is the original message to compare with (can be CRC or the actual original message)
        @param search_attempts_threshold    this is the number of search attempts before the decoder aborts and asks for more symbols
        @return a boolean and string, the first value indicating whether the search was successful, and the second the most likely message to have been transmitted from 
            the wavefront.
        """
        
        #reset everything
        self.edge_cost_average = 0
        self.edge_cost_count = 0
        
        num_spine_values = len(symbols)
        stack = [(0,0,[],0)]
        
        best_path = None
        num_search_attempts = 0
        cycles = 0

        #the Fano metric uses the rate as the bias
        num_symbols = 0
        for i in range(num_spine_values):
            num_symbols += len(symbols[i])        
        
        rate = self.k * num_spine_values / (1.0 * num_symbols)
        
        prob_avgs = []
        #print symbols
        
        while stack != []:
            cycles += 1
            
            best_path = max(stack, key = lambda x: x[0])
            
            (path_metric, spine_value, path, path_length) = best_path
            
            #print path_length
            
            if path_length == num_spine_values:
                candidate_message = self._decode_message(path)
                message_len = len(candidate_message)
                #do a diff
                diff = max([i for i in range(message_len) if candidate_message[:i] == original_message[:i]])
                #print "Number of cycles so far", cycles, " Candidate message: ",candidate_message.encode('hex'), " Original message: ", original_message.encode('hex')
                print "Number of cycles so far", cycles, " Agreement %d/%d" % (diff+1,message_len)
                if candidate_message == original_message:
                    return (True, candidate_message, cycles)
                    break
                
                #otherwise, we failed
                num_search_attempts += 1
                if num_search_attempts >= search_attempts_threshold:
                    return (False, None, cycles)
                
                stack.remove(best_path)
                continue
            
            # For each possible message block (2^k options)
            for edge in xrange( 1 << self.k ):
                # Calculate the new spine value
                new_spine_value = hash_func(spine_value, edge)
                
                # Get an RNG for the new spine value
                rng = RNG(new_spine_value)
                
                # Go over all received symbols, and compute the edge metric
                edge_cost = 0
                
                for received_symbol in symbols[path_length]:
                    # What the transmitter would have produced if it had this spine value
                    node_symbol = self.map_func(rng.next())
                    edge_cost += self._compute_edge_metric(node_symbol, received_symbol)
                    
                # The new path metric is the sum of all edges from the root
                new_path_metric = path_metric + edge_cost - self.edge_cost_average
                stack.append( (new_path_metric, new_spine_value, path + [edge], path_length + 1) )

            stack.remove(best_path)
            
        return (False, None, cycles)

    #######################
    # internal functions
    #######################
    def _compute_edge_metric(self, node_symbol, received_symbol):
        #what's the metric?
        cost = 0
        #keep track of the average cost
        
        if self.metric == "prob":
            cost = self._emit_log_prob(node_symbol, received_symbol)
            
        elif self.metric == "l2":
            distance = node_symbol - received_symbol
            cost = -distance*distance
        else:
            #throw an error
            raise RuntimeError, "Unsupported edge metric:  %s" % self.metric
    
        #this is the bias for the Fano metric
        if numpy.isfinite(cost):
            self.edge_cost_average = (self.edge_cost_count*self.edge_cost_average + cost)/(1.0 * self.edge_cost_count + 1)
            self.edge_cost_count += 1
        
        return cost    

    def _decode_message(self, path):
        # join the path, first element makes LSB, last element makes MSB
        message_as_number = 0
        for block in reversed(path):
            message_as_number = (message_as_number << self.k) | block

        # transform the integer representation into the binary message
        #??? + 7??
        n = (len(path) * self.k) / 8
        message = ""
        for i in xrange(n):
            message += chr(message_as_number & 0xFF)
            message_as_number >>= 8
            
        return message    
    
    def _integrate_gaussian(self, a,b, mu, sigma):
        def Phi(z):
            return 0.5*(1 + scipy.special.erf((z - mu)/(sigma*math.sqrt(2))))
            
        return Phi(b) - Phi(a)
        
    def _emit_log_prob(self, spinal_val, output):
        """
        Calculate the probability of seeing output symbol "output"
        given the spine_value (mapped to rng mapped to constellation), using our estimate of the noise
        """
        
        prob = self._integrate_gaussian(output-1,output+1,spinal_val,self.noise_std)
        if prob == 0:
            return -numpy.inf
        
        return numpy.log(prob)

        
