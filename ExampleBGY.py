# Copyright (c) 2012 Jonathan Perry
# This code is released under the MIT license (see LICENSE file).
'''
This is an example of the python implementation of spinal codes. A simple 
    "hello world" message is encoded, white gaussian noise with some SNR is 
    added, and the noisy data is then fed into the decoder.

This example produces constellation points with 8-bit precision. The noisy 
    symbols are quantized to integers, and all path metrics are done with
    integer arithmetic.
'''

# some constants:

k = 4
c = 8
precision = 14

#generate a random message
m = 256

# Message to be encoded:
message = ""

if __name__ == '__main__':
    from Encoder import Encoder
    from BGYDecoder import Decoder
    from SymbolMapper import SymbolMapper
    import random
    import math

    for i in range(m/8):
        message += chr(random.choice(range(256)))
    
    mapper = SymbolMapper(c, precision)
    map_func = lambda value: mapper.map(value)
    
    #Instantiate an encoder
    print 'Message string: "%s"' % message
    print 'Message hex:', message.encode("hex")
    encoder = Encoder(k, map_func, message)
    
    # spine length
    n = 8 * len(message)
    spine_length = (n + (k - 1)) / k
    
    # encode a short message, making 3 passes
    symbols = [encoder.get_symbol(i) for i in range(spine_length)*3]
    print "symbols: ", symbols

    # get average signal power
    signal_power = mapper.get_signal_average_power()
    
    # what is the noise power and standard deviation when SNR is 10dB?
    noise_power = signal_power / 10.0
    noise_std_dev = math.sqrt(noise_power)
    
    # add white gaussian noise at 10dB to signal
    print "Adding white gaussian noise at 10dB."
    noisy_symbols = [sym + random.gauss(0, noise_std_dev) for sym in symbols]
    # round to closest integer
    noisy_symbols = [int(x + 0.5) for x in noisy_symbols]
    print "noisy symbols:", noisy_symbols
    
    # instantiate decoder
    decoder = Decoder(k, map_func, noise_std_dev,"prob")
    symbols = []
    # update decoder with gathered points
    for i in xrange(spine_length):
        symbols.append([noisy_symbols[i], 
                         noisy_symbols[i+spine_length], 
                         noisy_symbols[i+2*spine_length]])
    
    ok, most_likely = decoder.decode(symbols,message,500)
    assert(ok), "Decoder failed too many times"
    
    print "decoded hex:", most_likely.encode("hex")
    print "decoded string:", most_likely.encode("hex").decode("hex")
    