# ViterbiDecoder
from Hash import hash_func
from RNG import RNG
import random
import numpy
import scipy
import scipy
import math

from scipy import integrate
from scipy import special

class Decoder(object):
    """
    Spinal decoder.
    
    To use this decoder to decode a message with a spine with n/k values, call
        advance() n/k times. On the i'th time, supplied parameter should be 
        a list of all symbols derived from the i'th spine value (ie using 
        encoder.get_symbol(i)).
         
    @note see advance() for assumption on inputs.
    """

    def __init__(self, k, B, map_func,noise_std):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param B beam width: the maximum number of sub-trees that the decoder
            keeps when advancing to next spine value
        @param d "depth": the number of layers in each of the sub-trees that
            the decoder keeps when advancing to next spine value
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.B = B
        self.map_func = map_func     
        self.symbols = []
        
        # self.wavefront is a list of tuples for each node in the wavefront.
        # Each tuple contains (path_metric, spine_value, path)
        #   where:
        #      path_metric is the sum of all edge weights from the root to the node
        #      spine_value is the spine value corresponding to the node
        #      path is a list of edges traversed from the root to the node.
        #         Each element in the list is a k-bit number (corresp. to the edge) 
        self.wavefront = [(0, 0, []) ]
        
        self.noise_std = noise_std
        
    def advance(self, symbols):
        """
        Advances the decoder to the next spine value, calculating weights using
            the given symbols. The given symbols can be noisy.

        @note The symbols supplied to advance() should be in the same order as 
            they were produced: 
            if the program ran
                a = enc.get_symbol(0)
                b = enc.get_symbol(0)
            then the first call to advance should be advance([a,b]), 
                not advance([b,a]).
        """
        
        #self.symbols.append(symbols)
        self._expand_wavefront(symbols)
        self._prune_wavefront()
    
    def get_most_likely(self):
        """
        @return a string, the most likely message to have been transmitted from 
            the wavefront.
        """
        # get the node with the smallest path metric. Note that this code breaks
        #   ties in favor of nodes with smaller spine value.
        path_metric, spine_value, path = max(self.wavefront)
        
        # join the path, first element makes LSB, last element makes MSB
        message_as_number = 0
        for block in reversed(path):
            message_as_number = (message_as_number << self.k) | block
        
        # transform the integer representation into the binary message
        n = (len(path) * self.k + 7) / 8
        message = ""
        for i in xrange(n):
            message += chr(message_as_number & 0xFF)
            message_as_number >>= 8

        return message 
    
    #######################
    # internal functions
    #######################
    def _integrate_gaussian(self, a,b, mu, sigma):
        def Phi(z):
            return 0.5*(1 + scipy.special.erf((z - mu)/(sigma*math.sqrt(2))))
            
        return Phi(b) - Phi(a)
        
    def _emit_log_prob(self, spinal_val, output):
        """
        Calculate the probability of seeing output symbol "output"
        given the spine_value (mapped to rng mapped to constellation), using our estimate of the noise
        """
        
        prob = self._integrate_gaussian(output-1,output+1,spinal_val,self.noise_std)
        if prob == 0:
            return -numpy.inf
        
        return numpy.log(prob)

        
    def _expand_wavefront(self, symbols):
        """
        Given the wavefront of the current spine location, and symbols produced
        from the next spine location, advance the wavefront to contain spine 
        values from the next spine location.
        """
        
        k = self.k
        
        new_wavefront = []
        # For each node in the current wavefront
        for (path_metric, spine_value, path) in self.wavefront:
            # For each possible message block (2^k options)
            for edge in xrange( 1 << k ):
                # Calculate the new spine value
                new_spine_value = hash_func(spine_value, edge)
                
                # Get an RNG for the new spine value
                rng = RNG(new_spine_value)
                
                # Go over all received symbols, and compute the edge metric
                edge_metric = 0
                for received_symbol in symbols:
                    # What the transmitter would have produced if it had this spine value
                    node_symbol = self.map_func(rng.next())
                    edge_metric += self._emit_log_prob(node_symbol,received_symbol)
                    
                if edge_metric != -numpy.inf:
                    # The new path metric is the sum of all edges from the root
                    new_path_metric = path_metric + edge_metric
                    # Add new node to wavefront
                    new_wavefront.append( (new_path_metric, new_spine_value, path + [edge]) )

        
        # Update the wavefront to the newly computed wavefront
        self.wavefront = new_wavefront

    def _prune_wavefront(self):
        """
        Given a wavefront, keep the B best paths
        """
        
        k = self.k
        B = self.B
        
        self.wavefront.sort(key = lambda x: x[0],reverse=True)
        self.wavefront = self.wavefront[:B]
        
    
    