# Copyright (c) 2012 Jonathan Perry
# This code is released under the MIT license (see LICENSE file).
from Encoder import Encoder

class PuncturedEncoder(object):
    """
    Spinal punctured encoder.
    This is a layer of abstraction of a barebones encoder that allows for implementation of arbitrary puncturing schedules
    
    """

    def __init__(self, k, map_func, message, num_tail_symbols, schedule):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        @param message a string with the message to be transmitted
        """
        self.k = k
        self.map_func = map_func
        self.num_tail_symbols = num_tail_symbols
        self.schedule_len = len(schedule)
        self.schedule = schedule
        self.encoder = Encoder(k,map_func,message)
        self.subpass = 0    #pointer to the current subpass
        
    def get_next_subpass(self):
        subpass_reduced = self.subpass % self.schedule_len
        offset = self.schedule[subpass_reduced]
        
        num_spine_values = self.encoder.get_num_spine_values()
        assert(num_spine_values % self.schedule_len == 0), "Schedule must divide number of spine values!"    
        num_blocks = num_spine_values/self.schedule_len
        
        subpass_symbols = {}
        
        #break the spines into blocks
        for i in range(num_blocks):
            index = i*self.schedule_len + offset
            subpass_symbols[index] = self.encoder.get_symbol(index)

        #if we're in subpass = 7 mod 8, then we emit a number of tail symbols
        tail_symbols = {}
        if subpass_reduced == self.schedule_len - 1:
            tail_symbols = [self.encoder.get_symbol(num_spine_values - 1) for i in range(self.num_tail_symbols - 1)]
            
        self.subpass += 1
        return (subpass_symbols,tail_symbols)

        
        