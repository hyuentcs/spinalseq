# ViterbiDecoder
from Hash import hash_func
from RNG import RNG
import random
import numpy
import scipy
import scipy
import math

from scipy import integrate
from scipy import special

class Decoder(object):
    """
    Spinal decoder.
    
    To use this decoder to decode a message with a spine with n/k values, call
        advance() n/k times. On the i'th time, supplied parameter should be 
        a list of all symbols derived from the i'th spine value (ie using 
        encoder.get_symbol(i)).
         
    @note see advance() for assumption on inputs.
    """

    def __init__(self, k, map_func,noise_std):
        """
        Constructor
        @param k the number of bits in each block fed into the hash function
        @param B beam width: the maximum number of sub-trees that the decoder
            keeps when advancing to next spine value
        @param d "depth": the number of layers in each of the sub-trees that
            the decoder keeps when advancing to next spine value
        @param map_func a function that maps a 16-bit pseudo-random number
             into a constellation point
        """
        self.k = k
        self.map_func = map_func     
        self.symbols = []
        self.stack = []
        
        # self.wavefront is a list of tuples for each node in the wavefront.
        # Each tuple contains (path_metric, spine_value, path)
        #   where:
        #      path_metric is the sum of all edge weights from the root to the node
        #      spine_value is the spine value corresponding to the node
        #      path is a list of edges traversed from the root to the node.
        #         Each element in the list is a k-bit number (corresp. to the edge) 
        self.wavefront = [(0, 0, []) ]
        
        self.noise_std = noise_std
        
    def advance(self, symbols):
        """
        Advances the decoder to the next spine value, calculating weights using
            the given symbols. The given symbols can be noisy.

        @note The symbols supplied to advance() should be in the same order as 
            they were produced: 
            if the program ran
                a = enc.get_symbol(0)
                b = enc.get_symbol(0)
            then the first call to advance should be advance([a,b]), 
                not advance([b,a]).
        """
        
        self.symbols.append(symbols)
        #self._expand_wavefront(symbols)
        #self._prune_wavefront()
    
    def get_most_likely(self,message):
        """
        @return a string, the most likely message to have been transmitted from 
            the wavefront.
        """
        
        n = len(self.symbols)
        self.stack = [(0,0,[])]
        
        best_path = None
        prob_avgs = []
        cycles = 0
        
        print "Original message: ",message
        
        while True:
            cycles += 1
            
            best_path = max(self.stack,key = lambda x: x[0])
            path_metric = best_path[0]
            spine_value = best_path[1]
            path = best_path[2]
            path_length = len(path)
            
            if path_length == n:
                print "Number of cycles so far", cycles
                candidate_message = self._decode_message(path)
                print "Candidate message: ",candidate_message
                if candidate_message == message:
                    return candidate_message
                    break
                else:
                    self.stack.remove(best_path)
                    continue
            
            #print path_length
            
            # For each possible message block (2^k options)
            for edge in xrange( 1 << self.k ):
                # Calculate the new spine value
                new_spine_value = hash_func(spine_value, edge)
                
                # Get an RNG for the new spine value
                rng = RNG(new_spine_value)
                
                # Go over all received symbols, and compute the edge metric
                edge_metric = 0
                for received_symbol in self.symbols[path_length]:
                    # What the transmitter would have produced if it had this spine value
                    node_symbol = self.map_func(rng.next())
                    prob = self._emit_log_prob(node_symbol,received_symbol)
                    edge_metric += prob
                    if prob != -numpy.inf:
                        prob_avgs.append(prob)
                    
                #print numpy.average(prob_avgs)
                
                if edge_metric != -numpy.inf:
                    # The new path metric is the sum of all edges from the root
                    #DON'T FORGET THE ANSWER TO LIFE AND STUFF: 12
                    new_path_metric = -3.5*numpy.average(prob_avgs) + path_metric + edge_metric
                    # Add new node to wavefront
                    self.stack.append( (new_path_metric, new_spine_value, path + [edge]) )            
        
            self.stack.remove(best_path)
            
    
    #######################
    # internal functions
    #######################
    def _decode_message(self, path):
        # join the path, first element makes LSB, last element makes MSB
        message_as_number = 0
        for block in reversed(path):
            message_as_number = (message_as_number << self.k) | block

        # transform the integer representation into the binary message
        n = (len(path) * self.k + 7) / 8
        message = ""
        for i in xrange(n):
            message += chr(message_as_number & 0xFF)
            message_as_number >>= 8
            
        return message    
    
    def _integrate_gaussian(self, a,b, mu, sigma):
        def Phi(z):
            return 0.5*(1 + scipy.special.erf((z - mu)/(sigma*math.sqrt(2))))
            
        return Phi(b) - Phi(a)
        
    def _emit_log_prob(self, spinal_val, output):
        """
        Calculate the probability of seeing output symbol "output"
        given the spine_value (mapped to rng mapped to constellation), using our estimate of the noise
        """
        
        prob = self._integrate_gaussian(output-1,output+1,spinal_val,self.noise_std)
        if prob == 0:
            return -numpy.inf
        
        return numpy.log(prob)

        
